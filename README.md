# daily-checklist

[![pipeline status](https://gitlab.com/Maarrk/daily-checklist/badges/master/pipeline.svg)](https://gitlab.com/Maarrk/daily-checklist/commits/master)

JS Generator for a simple PDF with dates to mark everyday tasks. Excercise in Vue 3 Composition API and D3.js

Deployed as [GitLab Page](https://maarrk.gitlab.io/daily-checklist/)
